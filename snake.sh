#!/bin/bash

mkdir -p tmp
length=1

function fruit() {
	heartRow=`echo "$(shuf -i 2-16 -n 1)"`
	heartCol=`echo "$(shuf -i 1-15 -n 1)"`
}

function board() {
	if [[ $1 == $heartRow && $2 == $heartCol ]]
	then
		length=$(($length+1))
		fruit
	fi
	sed -i -r "1 s/[0-9]/$length/" board.txt
	
	draw $1 $2
	
	if [[ $length > 1 ]]
	then
		count=1
		while [[ $count < $length ]]
		do
			if [[ $1 == ${snakeRow[$count]} && $2 == ${snakeCol[$count]} ]]
			then
				dead
			fi
			count=$(($count+1))
		done
	fi
	snakeRow=("$1" "${snakeRow[@]}")
	snakeCol=("$2" "${snakeCol[@]}")
	if [[ ${#snakeRow[@]} > $length ]]
	then
		erase ${snakeRow[-1]} ${snakeCol[-1]}
		unset snakeRow[-1]
		unset snakeCol[-1]
	fi
}

function draw() {
	sed -i -r "$(($1+1)) s/(^.{$(($(($2*2))-1))})../\1[]/" board.txt
	sed -i -r "$(($heartRow+1)) s/(^.{$(($(($heartCol*2))-1))})../\1<3/" board.txt
}

function erase() {
	sed -i -r "$(($1+1)) s/(^.{$(($(($2*2))-1))})../\1  /" board.txt
}

function dead() {
	clear
	cp copy.txt board.txt
	echo "You are dead"
	exit
}

function moveDown() { # arg 1 row, arg 2 col
	for ((i=$1;i<=17;i++))
	do
		if [[ $i == 17 ]]
		then
			dead
		fi
		board $i $2
		clear
		cat board.txt
		read -t 0.2 -n 1 key
		if [[ $key == 'd' || $key == 'a' ]]
		then
			control $i $2 $key
		fi
	done
}

function moveUp() { # arg 1 row, arg 2 col
	for ((i=$1;i>=1;i--))
	do
		if [[ $i == 1 ]]
		then
			dead
		fi
		board $i $2
		clear
		cat board.txt
		read -t 0.2 -n 1 key
		if [[ $key == 'a' || $key == 'd' ]]
		then
			control $i $2 $key
		fi
	done
}

function moveRight() { # arg 1 row, arg 2 col
	for ((i=$2;i<=16;i++))
	do
		if [[ $i == 16 ]]
		then
			dead
		fi
		board $1 $i
		clear
		cat board.txt
		read -t 0.2 -n 1 key
		if [[ $key == 'w' || $key == 's' ]]
		then
			control $1 $i $key
		fi
	done
}

function moveLeft() { # arg 1 row, arg 2 col
	for ((i=$2;i>=0;i--))
	do
		if [[ $i == 0 ]]
		then
			dead
		fi
		board $1 $i
		clear
		cat board.txt
		read -t 0.2 -n 1 key
		if [[ $key == 'w' || $key == 's' ]]
		then
			control $1 $i $key
		fi
	done
}

function control() {
	case "$3" in
		'w')
			moveUp $1 $2
			;;
		'a')
			moveLeft $1 $2
			;;
		's')
			moveDown $1 $2
			;;
		'd')
			moveRight $1 $2
			;;
	esac
}
fruit
control 8 8 'w'
